// Задание №1
function Student(name, point) {
    this.name = name;
    this.point = point;
    //this.show();
}
Student.prototype.constructor = Object.create(Array.prototype);
Student.prototype.show = function () {
    console.log('Студент ' + this.name + ' набрал ' + this.point + ' баллов');
};
var student = new Student('Тимур Вамуш', 30);
//console.log(student);

// Задание №2
//var studentsAndPoints = [
//    'Алексей Петров', 0,
//    'Ирина Овчинникова', 60,
//    'Глеб Стукалов', 30,
//    'Антон Павлович', 30,
//    'Виктория Заровская', 30,
//    'Алексей Левенец', 70,
//    'Тимур Вамуш', 30,
//    'Евгений Прочан', 60,
//    'Александр Малов', 0
//];
var studentsAndPoints = [
    new Student('Алексей Петров', 0),
    new Student('Ирина Овчинникова', 60),
    new Student('Глеб Стукалов', 30),
    new Student('Антон Павлович', 30),
    new Student('Виктория Заровская', 30),
    new Student('Алексей Левенец', 70),
    new Student('Тимур Вамуш', 30),
    new Student('Евгений Прочан', 60),
    new Student('Александр Малов', 0)
];

function StudentList(name, arr) {
    this.name = name;
    this.arr = arr;
}
StudentList.prototype = Object.create(Array.prototype);
StudentList.prototype.constructor = StudentList;
StudentList.prototype.add = function (studentName, studentPoint) {
    this.arr.push(new Student(studentName, studentPoint));
};

// Задание №3
var hj2 = new StudentList('HJ-2', studentsAndPoints);
//console.log(hj2);

// Задание №4
hj2.add('Вася Пупкин', 120);
hj2.add('Коля Супкин', 10);
hj2.add('Петя Печёнкин', 20);
//console.log(hj2);

// Задание №5
var html7 = new StudentList('HTML-7', []);
html7.add('Вася Пупкин II', 120);
html7.add('Коля Супкин II', 10);
html7.add('Петя Печёнкин II', 20);
//console.log(html7);

// Задание №6
StudentList.prototype.show = function () {
    console.log('Группа ' + this.name + ' (%s студентов)', (this.arr.length));
    //for(var i = 0; i < this.arr.length; i += 2) {
    //    console.log('Студент ' + this.arr[i] + ' набрал ' + this.arr[i + 1] + ' баллов');
    //}
    this.arr.forEach(function (student) {
        console.log('Студент ' + student.name + ' набрал ' + student.point + ' баллов');
    });
    console.log('');
};
hj2.show();
html7.show();

// Задание №7
StudentList.prototype.move = function (toGroup, studentName) {
    var studentIndex = this.arr.findIndex(function(x) {
        return x.name === studentName;
    });
    var movedStudent = this.arr.splice(studentIndex, 1);
    toGroup.arr.push(movedStudent[0]);
};
hj2.move(html7, 'Ирина Овчинникова');
//hj2.show();
//html7.show();

//// Дополнительное задание №8
//StudentList.prototype.max = function () {
//    var maxArray = [];
//    for(var i = 1; i < this.arr.length; i += 2) {
//        maxArray.push(this.arr[i]);
//    }
//    var max = Math.max.apply(null, maxArray);
//    var studentIndex = this.arr.findIndex(function(x) {
//        return x === max;
//    });
//    console.log('Студент %s имеет максимальное количество баллов в своей группе, равное %s', this.arr[studentIndex - 1], max);
//};
////hj2.max();

